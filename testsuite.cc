#include <iostream>
#include <complex>
#include <vector>

#include "poly.h"

size_t maxorder = 128;
bool verbose = false;

typedef double real_t;
typedef std::complex<real_t> complex_t;
typedef poly<real_t, complex_t> poly_t;

real_t pi = atan((real_t)1)*4;

real_t relative_error(real_t val, real_t est)
{
  return fabs((val-est)/val);
}

int test_error(poly_t p, real_t val, real_t maxerr)
{
  real_t M = p.maxmod(maxerr, verbose);
  real_t err = relative_error(val, M);

  if (err > maxerr)
    {
      std::cout
	<< p
	<< " error "
	<< err
	<< " > "
	<< maxerr
	<< std::endl;
      return 1;
    }

  return 0;
}

static poly_t unit_coeff_poly(size_t n)
{
  std::vector<complex_t> c(n);

  for (size_t i=0 ; i<n ; i++)
    {
      c[i] = 1.0;
    }

  poly_t p(c);

  return p;
}

static poly_t small_pert_poly(size_t n)
{
  std::vector<complex_t> c(n);

  c[0] = 1.0;
  c[1] = 1.0;
  c[n-1] = 1e-1;

  poly_t p(c);

  return p;
}

int main(void)
{
  size_t err = 0;
  std::vector<real_t> accs = {1e-3, 1e-6, 1e-9, 1e-12};

  std::cout << "unit coefficients" << std::endl;
  for (size_t i = 3 ; i < maxorder ; i++)
    {
      poly_t p = unit_coeff_poly(i);
      for (size_t j = 0 ; j < accs.size() ; j++)
        {
          err += test_error(p, i, accs[j]);
        }
    }

  std::cout << "small perturbation" << std::endl;
  for (size_t i=3 ; i<maxorder ; i++)
    {
      poly_t p = small_pert_poly(i);
      for (size_t j = 0 ; j < accs.size() ; j++)
        {
          err += test_error(p, p.l1norm(), accs[j]);
        }
    }

  std::cout << "total failures: " << err << std::endl;

  return (err ? EXIT_FAILURE : EXIT_SUCCESS);
}
